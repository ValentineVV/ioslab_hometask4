//
//  ViewController.m
//  UserForm
//
//  Created by Valiantsin Vasiliavitski on 4/4/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *reEnterPassword;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *nickname;
@property (weak, nonatomic) IBOutlet UITextField *eMail;
@property (weak, nonatomic) IBOutlet UITextField *phone;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.scrollView setKeyboardDismissMode:UIScrollViewKeyboardDismissModeInteractive];
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecogized)];
    [self.view addGestureRecognizer:recognizer];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag == 4 || textField.tag == 6) {
        [textField resignFirstResponder];
    } else {
        UITextField *nextField = [textField.superview viewWithTag:textField.tag + 1];
        if (nextField != nil) {
            [nextField becomeFirstResponder];
        }
    }
    return YES;
}

-(void)tapRecogized {
    [self.view endEditing:YES];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField.tag == 5 || textField.tag == 6){
        self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 220, 0);
        [self.scrollView setContentOffset:CGPointMake(0, 70)];
    }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
